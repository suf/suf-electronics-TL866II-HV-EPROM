EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L conn:Conn_02x12_Counter_Clockwise J1
U 1 1 60327BAB
P 3225 1375
F 0 "J1" H 3150 2000 50  0000 C CNN
F 1 "TL8266II" H 3525 2000 50  0000 C CNN
F 2 "Housings_DIP:DIP-24_W7.62mm" H 3225 1375 50  0001 C CNN
F 3 "~" H 3225 1375 50  0001 C CNN
	1    3225 1375
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_02x12_Counter_Clockwise J2
U 1 1 60328D4B
P 3225 3425
F 0 "J2" H 3150 4025 50  0000 C CNN
F 1 "ZIF" H 3350 4025 50  0000 C CNN
F 2 "Housings_DIP:DIP-24_W15.24mm_Socket" H 3225 3425 50  0001 C CNN
F 3 "~" H 3225 3425 50  0001 C CNN
	1    3225 3425
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 6032A011
P 2925 2050
F 0 "#PWR02" H 2925 1800 50  0001 C CNN
F 1 "GND" H 2930 1877 50  0000 C CNN
F 2 "" H 2925 2050 50  0001 C CNN
F 3 "" H 2925 2050 50  0001 C CNN
	1    2925 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3025 1975 2925 1975
Wire Wire Line
	2925 1975 2925 2050
$Comp
L power:GND #PWR03
U 1 1 6033034D
P 2925 4100
F 0 "#PWR03" H 2925 3850 50  0001 C CNN
F 1 "GND" H 2930 3927 50  0000 C CNN
F 2 "" H 2925 4100 50  0001 C CNN
F 3 "" H 2925 4100 50  0001 C CNN
	1    2925 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3025 4025 2925 4025
Wire Wire Line
	2925 4025 2925 4100
$Comp
L Device:D_Schottky_Small D12
U 1 1 6033085F
P 3625 2600
F 0 "D12" V 3700 2775 50  0000 R CNN
F 1 "1PS76SB10" V 3525 3075 50  0000 R CNN
F 2 "Diodes_SMD:D_SOD-323_HandSoldering" V 3625 2600 50  0001 C CNN
F 3 "~" V 3625 2600 50  0001 C CNN
	1    3625 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3525 875  3625 875 
Wire Wire Line
	3625 875  3625 2500
Wire Wire Line
	3625 2700 3625 2925
Wire Wire Line
	3625 2925 3525 2925
$Comp
L Device:D_Zener_Small D13
U 1 1 60332615
P 4800 975
F 0 "D13" H 4650 1025 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 5200 1025 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 4800 975 50  0001 C CNN
F 3 "~" V 4800 975 50  0001 C CNN
	1    4800 975 
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R12
U 1 1 60334D1A
P 3700 2325
F 0 "R12" V 3750 2125 50  0000 L CNN
F 1 "100" V 3750 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3700 2325 50  0001 C CNN
F 3 "~" H 3700 2325 50  0001 C CNN
	1    3700 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R13
U 1 1 6033706E
P 3800 2325
F 0 "R13" V 3850 2125 50  0000 L CNN
F 1 "100" V 3850 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3800 2325 50  0001 C CNN
F 3 "~" H 3800 2325 50  0001 C CNN
	1    3800 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R14
U 1 1 6033762F
P 3900 2325
F 0 "R14" V 3950 2125 50  0000 L CNN
F 1 "100" V 3950 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3900 2325 50  0001 C CNN
F 3 "~" H 3900 2325 50  0001 C CNN
	1    3900 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R15
U 1 1 60337639
P 4000 2325
F 0 "R15" V 4050 2125 50  0000 L CNN
F 1 "100" V 4050 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4000 2325 50  0001 C CNN
F 3 "~" H 4000 2325 50  0001 C CNN
	1    4000 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R16
U 1 1 603380D3
P 4100 2325
F 0 "R16" V 4150 2125 50  0000 L CNN
F 1 "100" V 4150 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4100 2325 50  0001 C CNN
F 3 "~" H 4100 2325 50  0001 C CNN
	1    4100 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R17
U 1 1 603380DD
P 4200 2325
F 0 "R17" V 4250 2125 50  0000 L CNN
F 1 "100" V 4250 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4200 2325 50  0001 C CNN
F 3 "~" H 4200 2325 50  0001 C CNN
	1    4200 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R18
U 1 1 603380E7
P 4300 2325
F 0 "R18" V 4350 2125 50  0000 L CNN
F 1 "100" V 4350 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4300 2325 50  0001 C CNN
F 3 "~" H 4300 2325 50  0001 C CNN
	1    4300 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R19
U 1 1 603380F1
P 4400 2325
F 0 "R19" V 4450 2125 50  0000 L CNN
F 1 "100" V 4450 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4400 2325 50  0001 C CNN
F 3 "~" H 4400 2325 50  0001 C CNN
	1    4400 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R20
U 1 1 603392B7
P 4500 2325
F 0 "R20" V 4550 2125 50  0000 L CNN
F 1 "100" V 4550 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4500 2325 50  0001 C CNN
F 3 "~" H 4500 2325 50  0001 C CNN
	1    4500 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R21
U 1 1 603392C1
P 4600 2325
F 0 "R21" V 4650 2125 50  0000 L CNN
F 1 "100" V 4650 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4600 2325 50  0001 C CNN
F 3 "~" H 4600 2325 50  0001 C CNN
	1    4600 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 60339E77
P 1850 2325
F 0 "R2" V 1900 2125 50  0000 L CNN
F 1 "100" V 1900 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 1850 2325 50  0001 C CNN
F 3 "~" H 1850 2325 50  0001 C CNN
	1    1850 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 60339E81
P 1950 2325
F 0 "R3" V 2000 2125 50  0000 L CNN
F 1 "100" V 2000 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 1950 2325 50  0001 C CNN
F 3 "~" H 1950 2325 50  0001 C CNN
	1    1950 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R4
U 1 1 60339E8B
P 2050 2325
F 0 "R4" V 2100 2125 50  0000 L CNN
F 1 "100" V 2100 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2050 2325 50  0001 C CNN
F 3 "~" H 2050 2325 50  0001 C CNN
	1    2050 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R5
U 1 1 60339E95
P 2150 2325
F 0 "R5" V 2200 2125 50  0000 L CNN
F 1 "100" V 2200 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2150 2325 50  0001 C CNN
F 3 "~" H 2150 2325 50  0001 C CNN
	1    2150 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R6
U 1 1 60339E9F
P 2250 2325
F 0 "R6" V 2300 2125 50  0000 L CNN
F 1 "100" V 2300 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2250 2325 50  0001 C CNN
F 3 "~" H 2250 2325 50  0001 C CNN
	1    2250 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R7
U 1 1 60339EA9
P 2350 2325
F 0 "R7" V 2400 2125 50  0000 L CNN
F 1 "100" V 2400 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2350 2325 50  0001 C CNN
F 3 "~" H 2350 2325 50  0001 C CNN
	1    2350 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R8
U 1 1 60339EB3
P 2450 2325
F 0 "R8" V 2500 2125 50  0000 L CNN
F 1 "100" V 2500 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2450 2325 50  0001 C CNN
F 3 "~" H 2450 2325 50  0001 C CNN
	1    2450 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R9
U 1 1 60339EBD
P 2550 2325
F 0 "R9" V 2600 2125 50  0000 L CNN
F 1 "100" V 2600 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2550 2325 50  0001 C CNN
F 3 "~" H 2550 2325 50  0001 C CNN
	1    2550 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R10
U 1 1 60339EC7
P 2650 2325
F 0 "R10" V 2700 2125 50  0000 L CNN
F 1 "100" V 2700 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2650 2325 50  0001 C CNN
F 3 "~" H 2650 2325 50  0001 C CNN
	1    2650 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R11
U 1 1 60339ED1
P 2750 2325
F 0 "R11" V 2800 2125 50  0000 L CNN
F 1 "100" V 2800 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2750 2325 50  0001 C CNN
F 3 "~" H 2750 2325 50  0001 C CNN
	1    2750 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 6033FA65
P 1750 2325
F 0 "R1" V 1800 2125 50  0000 L CNN
F 1 "100" V 1800 2425 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 1750 2325 50  0001 C CNN
F 3 "~" H 1750 2325 50  0001 C CNN
	1    1750 2325
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D14
U 1 1 60340063
P 4800 1075
F 0 "D14" H 4650 1125 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 5200 1125 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 4800 1075 50  0001 C CNN
F 3 "~" V 4800 1075 50  0001 C CNN
	1    4800 1075
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D15
U 1 1 60340674
P 4800 1275
F 0 "D15" H 4650 1325 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 5200 1325 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 4800 1275 50  0001 C CNN
F 3 "~" V 4800 1275 50  0001 C CNN
	1    4800 1275
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D16
U 1 1 60341716
P 4800 1375
F 0 "D16" H 4650 1425 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 5200 1425 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 4800 1375 50  0001 C CNN
F 3 "~" V 4800 1375 50  0001 C CNN
	1    4800 1375
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D17
U 1 1 60341720
P 4800 1475
F 0 "D17" H 4650 1525 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 5200 1525 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 4800 1475 50  0001 C CNN
F 3 "~" V 4800 1475 50  0001 C CNN
	1    4800 1475
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D18
U 1 1 6034172A
P 4800 1575
F 0 "D18" H 4650 1625 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 5200 1625 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 4800 1575 50  0001 C CNN
F 3 "~" V 4800 1575 50  0001 C CNN
	1    4800 1575
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D19
U 1 1 60341734
P 4800 1675
F 0 "D19" H 4650 1725 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 5200 1725 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 4800 1675 50  0001 C CNN
F 3 "~" V 4800 1675 50  0001 C CNN
	1    4800 1675
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D20
U 1 1 6034C726
P 4800 1775
F 0 "D20" H 4650 1825 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 5200 1825 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 4800 1775 50  0001 C CNN
F 3 "~" V 4800 1775 50  0001 C CNN
	1    4800 1775
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D21
U 1 1 6034C730
P 4800 1875
F 0 "D21" H 4650 1925 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 5200 1925 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 4800 1875 50  0001 C CNN
F 3 "~" V 4800 1875 50  0001 C CNN
	1    4800 1875
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D22
U 1 1 6034C73A
P 4800 1975
F 0 "D22" H 4650 2025 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 5200 2025 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 4800 1975 50  0001 C CNN
F 3 "~" V 4800 1975 50  0001 C CNN
	1    4800 1975
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D1
U 1 1 6034DD01
P 1550 875
F 0 "D1" H 1400 925 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 925 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 875 50  0001 C CNN
F 3 "~" V 1550 875 50  0001 C CNN
	1    1550 875 
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D2
U 1 1 6034DD0B
P 1550 975
F 0 "D2" H 1400 1025 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 1025 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 975 50  0001 C CNN
F 3 "~" V 1550 975 50  0001 C CNN
	1    1550 975 
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D3
U 1 1 6034DD15
P 1550 1075
F 0 "D3" H 1400 1125 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 1125 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 1075 50  0001 C CNN
F 3 "~" V 1550 1075 50  0001 C CNN
	1    1550 1075
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D4
U 1 1 6034DD1F
P 1550 1175
F 0 "D4" H 1400 1225 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 1225 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 1175 50  0001 C CNN
F 3 "~" V 1550 1175 50  0001 C CNN
	1    1550 1175
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D5
U 1 1 6034DD29
P 1550 1275
F 0 "D5" H 1400 1325 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 1325 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 1275 50  0001 C CNN
F 3 "~" V 1550 1275 50  0001 C CNN
	1    1550 1275
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D6
U 1 1 6034DD33
P 1550 1375
F 0 "D6" H 1400 1425 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 1425 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 1375 50  0001 C CNN
F 3 "~" V 1550 1375 50  0001 C CNN
	1    1550 1375
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D7
U 1 1 6034DD3D
P 1550 1475
F 0 "D7" H 1400 1525 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 1525 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 1475 50  0001 C CNN
F 3 "~" V 1550 1475 50  0001 C CNN
	1    1550 1475
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D8
U 1 1 6034DD47
P 1550 1575
F 0 "D8" H 1400 1625 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 1625 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 1575 50  0001 C CNN
F 3 "~" V 1550 1575 50  0001 C CNN
	1    1550 1575
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D9
U 1 1 6034DD51
P 1550 1675
F 0 "D9" H 1400 1725 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 1725 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 1675 50  0001 C CNN
F 3 "~" V 1550 1675 50  0001 C CNN
	1    1550 1675
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D10
U 1 1 6034DD5B
P 1550 1775
F 0 "D10" H 1400 1825 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 1825 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 1775 50  0001 C CNN
F 3 "~" V 1550 1775 50  0001 C CNN
	1    1550 1775
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_Small D11
U 1 1 6034DD65
P 1550 1875
F 0 "D11" H 1400 1925 50  0000 C CNN
F 1 "BZT52C6V8V-GS08" H 1950 1925 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" V 1550 1875 50  0001 C CNN
F 3 "~" V 1550 1875 50  0001 C CNN
	1    1550 1875
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3525 975  3700 975 
Wire Wire Line
	3525 1075 3800 1075
Wire Wire Line
	3525 1275 3900 1275
Wire Wire Line
	3525 1375 4000 1375
Wire Wire Line
	3525 1475 4100 1475
Wire Wire Line
	3525 1575 4200 1575
Wire Wire Line
	3525 1675 4300 1675
Wire Wire Line
	3525 1775 4400 1775
Wire Wire Line
	3525 1875 4500 1875
Wire Wire Line
	3525 1975 4600 1975
Wire Wire Line
	3700 2225 3700 975 
Connection ~ 3700 975 
Wire Wire Line
	3700 975  4700 975 
Wire Wire Line
	3800 2225 3800 1075
Connection ~ 3800 1075
Wire Wire Line
	3800 1075 4700 1075
Wire Wire Line
	3900 2225 3900 1275
Connection ~ 3900 1275
Wire Wire Line
	3900 1275 4700 1275
Wire Wire Line
	4000 2225 4000 1375
Connection ~ 4000 1375
Wire Wire Line
	4000 1375 4700 1375
Wire Wire Line
	4100 2225 4100 1475
Connection ~ 4100 1475
Wire Wire Line
	4100 1475 4700 1475
Wire Wire Line
	4200 2225 4200 1575
Connection ~ 4200 1575
Wire Wire Line
	4200 1575 4700 1575
Wire Wire Line
	4300 2225 4300 1675
Connection ~ 4300 1675
Wire Wire Line
	4300 1675 4700 1675
Wire Wire Line
	4400 2225 4400 1775
Connection ~ 4400 1775
Wire Wire Line
	4400 1775 4700 1775
Wire Wire Line
	4500 2225 4500 1875
Connection ~ 4500 1875
Wire Wire Line
	4500 1875 4700 1875
Wire Wire Line
	4600 2225 4600 1975
Connection ~ 4600 1975
Wire Wire Line
	4600 1975 4700 1975
Wire Wire Line
	3700 2425 3700 3025
Wire Wire Line
	3700 3025 3525 3025
Wire Wire Line
	3800 2425 3800 3125
Wire Wire Line
	3800 3125 3525 3125
Wire Wire Line
	3900 2425 3900 3325
Wire Wire Line
	3900 3325 3525 3325
Wire Wire Line
	4000 2425 4000 3425
Wire Wire Line
	4000 3425 3525 3425
Wire Wire Line
	4100 2425 4100 3525
Wire Wire Line
	4100 3525 3525 3525
Wire Wire Line
	4200 2425 4200 3625
Wire Wire Line
	4200 3625 3525 3625
Wire Wire Line
	4300 2425 4300 3725
Wire Wire Line
	4300 3725 3525 3725
Wire Wire Line
	4400 2425 4400 3825
Wire Wire Line
	4400 3825 3525 3825
Wire Wire Line
	4500 2425 4500 3925
Wire Wire Line
	4500 3925 3525 3925
Wire Wire Line
	4600 2425 4600 4025
Wire Wire Line
	4600 4025 3525 4025
Wire Wire Line
	2750 2425 2750 2925
Wire Wire Line
	2750 2925 3025 2925
Wire Wire Line
	2650 2425 2650 3025
Wire Wire Line
	2650 3025 3025 3025
Wire Wire Line
	2550 2425 2550 3125
Wire Wire Line
	2550 3125 3025 3125
Wire Wire Line
	2450 2425 2450 3225
Wire Wire Line
	2450 3225 3025 3225
Wire Wire Line
	2350 2425 2350 3325
Wire Wire Line
	2350 3325 3025 3325
Wire Wire Line
	3025 3425 2250 3425
Wire Wire Line
	2250 3425 2250 2425
Wire Wire Line
	2150 2425 2150 3525
Wire Wire Line
	2150 3525 3025 3525
Wire Wire Line
	3025 3625 2050 3625
Wire Wire Line
	2050 3625 2050 2425
Wire Wire Line
	1950 2425 1950 3725
Wire Wire Line
	1950 3725 3025 3725
Wire Wire Line
	3025 3825 1850 3825
Wire Wire Line
	1850 3825 1850 2425
Wire Wire Line
	1750 2425 1750 3925
Wire Wire Line
	1750 3925 3025 3925
Wire Wire Line
	1650 875  2750 875 
Wire Wire Line
	3025 975  2650 975 
Wire Wire Line
	1650 1075 2550 1075
Wire Wire Line
	3025 1175 2450 1175
Wire Wire Line
	1650 1275 2350 1275
Wire Wire Line
	3025 1375 2250 1375
Wire Wire Line
	1650 1475 2150 1475
Wire Wire Line
	3025 1575 2050 1575
Wire Wire Line
	1650 1675 1950 1675
Wire Wire Line
	3025 1775 1850 1775
Wire Wire Line
	1650 1875 1750 1875
Wire Wire Line
	2750 2225 2750 875 
Connection ~ 2750 875 
Wire Wire Line
	2750 875  3025 875 
Wire Wire Line
	2650 2225 2650 975 
Connection ~ 2650 975 
Wire Wire Line
	2650 975  1650 975 
Wire Wire Line
	2550 2225 2550 1075
Connection ~ 2550 1075
Wire Wire Line
	2550 1075 3025 1075
Wire Wire Line
	2450 2225 2450 1175
Connection ~ 2450 1175
Wire Wire Line
	2450 1175 1650 1175
Wire Wire Line
	2350 2225 2350 1275
Connection ~ 2350 1275
Wire Wire Line
	2350 1275 3025 1275
Wire Wire Line
	2250 2225 2250 1375
Connection ~ 2250 1375
Wire Wire Line
	2250 1375 1650 1375
Wire Wire Line
	2150 2225 2150 1475
Connection ~ 2150 1475
Wire Wire Line
	2150 1475 3025 1475
Wire Wire Line
	2050 2225 2050 1575
Connection ~ 2050 1575
Wire Wire Line
	2050 1575 1650 1575
Wire Wire Line
	1950 2225 1950 1675
Connection ~ 1950 1675
Wire Wire Line
	1950 1675 3025 1675
Wire Wire Line
	1850 2225 1850 1775
Connection ~ 1850 1775
Wire Wire Line
	1850 1775 1650 1775
Wire Wire Line
	1750 2225 1750 1875
Connection ~ 1750 1875
Wire Wire Line
	1750 1875 3025 1875
$Comp
L power:GND #PWR04
U 1 1 603E921A
P 5625 2050
F 0 "#PWR04" H 5625 1800 50  0001 C CNN
F 1 "GND" H 5630 1877 50  0000 C CNN
F 2 "" H 5625 2050 50  0001 C CNN
F 3 "" H 5625 2050 50  0001 C CNN
	1    5625 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 975  5625 975 
Wire Wire Line
	5625 975  5625 1075
$Comp
L power:GND #PWR01
U 1 1 603EE4EB
P 750 2000
F 0 "#PWR01" H 750 1750 50  0001 C CNN
F 1 "GND" H 755 1827 50  0000 C CNN
F 2 "" H 750 2000 50  0001 C CNN
F 3 "" H 750 2000 50  0001 C CNN
	1    750  2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  875  750  975 
Wire Wire Line
	750  875  1450 875 
Wire Wire Line
	1450 975  750  975 
Connection ~ 750  975 
Wire Wire Line
	750  975  750  1075
Wire Wire Line
	1450 1075 750  1075
Connection ~ 750  1075
Wire Wire Line
	750  1075 750  1175
Wire Wire Line
	1450 1175 750  1175
Connection ~ 750  1175
Wire Wire Line
	750  1175 750  1275
Wire Wire Line
	1450 1275 750  1275
Connection ~ 750  1275
Wire Wire Line
	750  1275 750  1375
Wire Wire Line
	1450 1375 750  1375
Connection ~ 750  1375
Wire Wire Line
	750  1375 750  1475
Wire Wire Line
	1450 1475 750  1475
Connection ~ 750  1475
Wire Wire Line
	750  1475 750  1575
Wire Wire Line
	1450 1575 750  1575
Connection ~ 750  1575
Wire Wire Line
	750  1575 750  1675
Wire Wire Line
	1450 1675 750  1675
Connection ~ 750  1675
Wire Wire Line
	750  1675 750  1775
Wire Wire Line
	1450 1775 750  1775
Connection ~ 750  1775
Wire Wire Line
	750  1775 750  1875
Wire Wire Line
	1450 1875 750  1875
Connection ~ 750  1875
Wire Wire Line
	750  1875 750  2000
Wire Wire Line
	4900 1075 5625 1075
Connection ~ 5625 1075
Wire Wire Line
	5625 1075 5625 1275
Wire Wire Line
	4900 1275 5625 1275
Connection ~ 5625 1275
Wire Wire Line
	5625 1275 5625 1375
Wire Wire Line
	4900 1375 5625 1375
Connection ~ 5625 1375
Wire Wire Line
	5625 1375 5625 1475
Wire Wire Line
	4900 1475 5625 1475
Connection ~ 5625 1475
Wire Wire Line
	5625 1475 5625 1575
Wire Wire Line
	4900 1575 5625 1575
Connection ~ 5625 1575
Wire Wire Line
	5625 1575 5625 1675
Wire Wire Line
	4900 1675 5625 1675
Connection ~ 5625 1675
Wire Wire Line
	5625 1675 5625 1775
Wire Wire Line
	4900 1775 5625 1775
Connection ~ 5625 1775
Wire Wire Line
	5625 1775 5625 1875
Wire Wire Line
	4900 1875 5625 1875
Connection ~ 5625 1875
Wire Wire Line
	5625 1875 5625 1975
Wire Wire Line
	4900 1975 5625 1975
Connection ~ 5625 1975
Wire Wire Line
	5625 1975 5625 2050
$Comp
L suf_regulator:MC34063 U1
U 1 1 6046E340
P 8800 1400
F 0 "U1" H 8600 1675 60  0000 C CNN
F 1 "MC34063" H 9075 1675 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 8650 1400 60  0001 C CNN
F 3 "" H 8650 1400 60  0001 C CNN
	1    8800 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R24
U 1 1 6046F472
P 8325 1925
F 0 "R24" H 8384 1971 50  0000 L CNN
F 1 "1,5K" H 8384 1880 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 8325 1925 50  0001 C CNN
F 3 "~" H 8325 1925 50  0001 C CNN
	1    8325 1925
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R25
U 1 1 604700F7
P 8600 1750
F 0 "R25" V 8550 1575 50  0000 C CNN
F 1 "24K" V 8675 1800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 8600 1750 50  0001 C CNN
F 3 "~" H 8600 1750 50  0001 C CNN
	1    8600 1750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R26
U 1 1 60470C68
P 9050 1750
F 0 "R26" V 8975 1600 50  0000 C CNN
F 1 "4,7K" V 9125 1750 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9050 1750 50  0001 C CNN
F 3 "~" H 9050 1750 50  0001 C CNN
	1    9050 1750
	0    1    1    0   
$EndComp
$Comp
L Device:CP_Small C1
U 1 1 60471E9F
P 8200 1925
F 0 "C1" H 8000 1950 50  0000 L CNN
F 1 "100uF/16V" H 7675 1850 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_6.3x5.7" H 8200 1925 50  0001 C CNN
F 3 "~" H 8200 1925 50  0001 C CNN
	1    8200 1925
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R23
U 1 1 60472FBC
P 8075 1450
F 0 "R23" V 8125 1225 50  0000 L CNN
F 1 "0,22" V 8025 1525 50  0000 L CNN
F 2 "Resistors_SMD:R_2010_HandSoldering" H 8075 1450 50  0001 C CNN
F 3 "~" H 8075 1450 50  0001 C CNN
	1    8075 1450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R22
U 1 1 60473CC9
P 8075 1250
F 0 "R22" V 8025 1075 50  0000 C CNN
F 1 "180" V 8025 1400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 8075 1250 50  0001 C CNN
F 3 "~" H 8075 1250 50  0001 C CNN
	1    8075 1250
	0    1    1    0   
$EndComp
$Comp
L Device:CP_Small C3
U 1 1 60474BAA
P 9625 1925
F 0 "C3" H 9713 1971 50  0000 L CNN
F 1 "330uF/35V" H 9475 1700 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 9625 1925 50  0001 C CNN
F 3 "~" H 9625 1925 50  0001 C CNN
	1    9625 1925
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 60475A55
P 9425 1925
F 0 "C2" H 9200 2000 50  0000 L CNN
F 1 "1,5nF" H 9100 1875 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9425 1925 50  0001 C CNN
F 3 "~" H 9425 1925 50  0001 C CNN
	1    9425 1925
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky_Small D23
U 1 1 60477B6F
P 9625 1450
F 0 "D23" V 9671 1382 50  0000 R CNN
F 1 "SS26" V 9550 1450 50  0000 R CNN
F 2 "Diodes_SMD:D_SMB_Handsoldering" V 9625 1450 50  0001 C CNN
F 3 "~" V 9625 1450 50  0001 C CNN
	1    9625 1450
	0    -1   -1   0   
$EndComp
$Comp
L Device:L_Small L1
U 1 1 60478ABF
P 8800 1025
F 0 "L1" V 8985 1025 50  0000 C CNN
F 1 "220uH" V 8894 1025 50  0000 C CNN
F 2 "Choke_SMD:Choke_SMD_12x12mm_h6mm" H 8800 1025 50  0001 C CNN
F 3 "~" H 8800 1025 50  0001 C CNN
	1    8800 1025
	0    -1   -1   0   
$EndComp
$Comp
L power:+12V #PWR05
U 1 1 60488BC9
P 7675 1575
F 0 "#PWR05" H 7675 1425 50  0001 C CNN
F 1 "+12V" H 7690 1748 50  0000 C CNN
F 2 "" H 7675 1575 50  0001 C CNN
F 3 "" H 7675 1575 50  0001 C CNN
	1    7675 1575
	1    0    0    -1  
$EndComp
Wire Wire Line
	7975 1250 7975 1350
Wire Wire Line
	8175 1450 8200 1450
Wire Wire Line
	8350 1350 7975 1350
Connection ~ 7975 1350
Wire Wire Line
	7975 1350 7975 1450
Wire Wire Line
	8175 1250 8350 1250
Wire Wire Line
	8700 1025 7975 1025
Wire Wire Line
	7975 1025 7975 1250
Connection ~ 7975 1250
Wire Wire Line
	8900 1025 9625 1025
Wire Wire Line
	9625 1025 9625 1250
Wire Wire Line
	7675 1575 8200 1575
Wire Wire Line
	8200 1575 8200 1825
Wire Wire Line
	8200 1575 8200 1450
Connection ~ 8200 1575
Connection ~ 8200 1450
Wire Wire Line
	8200 1450 8350 1450
Wire Wire Line
	8325 1825 8325 1750
Wire Wire Line
	8325 1550 8350 1550
Wire Wire Line
	8500 1750 8325 1750
Connection ~ 8325 1750
Wire Wire Line
	8325 1750 8325 1550
Wire Wire Line
	8700 1750 8825 1750
Wire Wire Line
	9625 1750 9625 1550
Wire Wire Line
	9150 1750 9625 1750
Wire Wire Line
	9425 1825 9425 1450
Wire Wire Line
	9425 1450 9250 1450
Wire Wire Line
	9250 1350 9325 1350
Wire Wire Line
	9325 1350 9325 1550
Wire Wire Line
	9425 2100 9425 2025
Wire Wire Line
	9250 1550 9325 1550
Connection ~ 9325 1550
Wire Wire Line
	9325 1550 9325 2100
$Comp
L power:GND #PWR07
U 1 1 60548A47
P 9425 2150
F 0 "#PWR07" H 9425 1900 50  0001 C CNN
F 1 "GND" H 9430 1977 50  0000 C CNN
F 2 "" H 9425 2150 50  0001 C CNN
F 3 "" H 9425 2150 50  0001 C CNN
	1    9425 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 1250 9625 1250
Connection ~ 9625 1250
Wire Wire Line
	9625 1250 9625 1350
Wire Wire Line
	9425 2100 9625 2100
Wire Wire Line
	9625 2100 9625 2025
Connection ~ 9425 2100
Wire Wire Line
	9425 2100 9425 2150
Wire Wire Line
	9325 2100 9425 2100
Wire Wire Line
	9625 1750 9625 1825
Connection ~ 9625 1750
$Comp
L power:VPP #PWR08
U 1 1 6058CDCE
P 9900 1650
F 0 "#PWR08" H 9900 1500 50  0001 C CNN
F 1 "VPP" H 9915 1823 50  0000 C CNN
F 2 "" H 9900 1650 50  0001 C CNN
F 3 "" H 9900 1650 50  0001 C CNN
	1    9900 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9625 1750 9900 1750
Wire Wire Line
	9900 1750 9900 1650
$Comp
L power:GND #PWR06
U 1 1 60597D04
P 8250 2100
F 0 "#PWR06" H 8250 1850 50  0001 C CNN
F 1 "GND" H 8255 1927 50  0000 C CNN
F 2 "" H 8250 2100 50  0001 C CNN
F 3 "" H 8250 2100 50  0001 C CNN
	1    8250 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 2025 8200 2075
Wire Wire Line
	8200 2075 8250 2075
Wire Wire Line
	8325 2075 8325 2025
Wire Wire Line
	8250 2100 8250 2075
Connection ~ 8250 2075
Wire Wire Line
	8250 2075 8325 2075
$Comp
L Switch:SW_DIP_x01 SW1
U 1 1 605C26A6
P 8825 2125
F 0 "SW1" V 8779 2255 50  0000 L CNN
F 1 "21/25V" V 8870 2255 50  0000 L CNN
F 2 "Buttons_Switches_ThroughHole:SW_DIP_x1_W7.62mm_Slide" H 8825 2125 50  0001 C CNN
F 3 "~" H 8825 2125 50  0001 C CNN
	1    8825 2125
	0    1    1    0   
$EndComp
Wire Wire Line
	8825 1750 8825 1825
Connection ~ 8825 1750
Wire Wire Line
	8825 1750 8950 1750
Wire Wire Line
	8825 2425 9900 2425
Wire Wire Line
	9900 2425 9900 1750
Connection ~ 9900 1750
$Comp
L Device:Q_PNP_BEC Q1
U 1 1 605D994D
P 8500 3025
F 0 "Q1" V 8828 3025 50  0000 C CNN
F 1 "MMBTA56LT1G" V 8737 3025 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 8700 3125 50  0001 C CNN
F 3 "~" H 8500 3025 50  0001 C CNN
	1    8500 3025
	0    -1   -1   0   
$EndComp
Text GLabel 4700 3225 2    50   Input ~ 0
VPROG
Wire Wire Line
	3525 3225 4700 3225
Text GLabel 3900 1175 2    50   Input ~ 0
VIN
Wire Wire Line
	3525 1175 3900 1175
$Comp
L power:VPP #PWR012
U 1 1 605F39E5
P 8925 2850
F 0 "#PWR012" H 8925 2700 50  0001 C CNN
F 1 "VPP" H 8940 3023 50  0000 C CNN
F 2 "" H 8925 2850 50  0001 C CNN
F 3 "" H 8925 2850 50  0001 C CNN
	1    8925 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R29
U 1 1 605F466F
P 8775 3275
F 0 "R29" V 8850 3275 50  0000 C CNN
F 1 "1K" V 8700 3275 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 8775 3275 50  0001 C CNN
F 3 "~" H 8775 3275 50  0001 C CNN
	1    8775 3275
	0    1    1    0   
$EndComp
Wire Wire Line
	8925 2850 8925 2925
Wire Wire Line
	8925 3275 8875 3275
Wire Wire Line
	8700 2925 8925 2925
Connection ~ 8925 2925
Wire Wire Line
	8925 2925 8925 3275
Wire Wire Line
	8500 3225 8500 3275
Wire Wire Line
	8500 3275 8675 3275
Text GLabel 7850 2925 0    50   Input ~ 0
VPROG
Wire Wire Line
	7850 2925 7950 2925
$Comp
L Device:D_Schottky_Small D24
U 1 1 60622E19
P 7950 3125
F 0 "D24" V 7950 3175 50  0000 L CNN
F 1 "1PS76SB10" V 7950 2625 50  0000 L CNN
F 2 "Diodes_SMD:D_SOD-323_HandSoldering" V 7950 3125 50  0001 C CNN
F 3 "~" V 7950 3125 50  0001 C CNN
	1    7950 3125
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q2
U 1 1 606320C9
P 8600 3925
F 0 "Q2" H 8791 3971 50  0000 L CNN
F 1 "BC846ALT1" H 8791 3880 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 8800 4025 50  0001 C CNN
F 3 "~" H 8600 3925 50  0001 C CNN
	1    8600 3925
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R28
U 1 1 606402AC
P 8500 3500
F 0 "R28" V 8575 3500 50  0000 C CNN
F 1 "12K" V 8425 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 8500 3500 50  0001 C CNN
F 3 "~" H 8500 3500 50  0001 C CNN
	1    8500 3500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R27
U 1 1 60640D92
P 7950 3425
F 0 "R27" V 8025 3425 50  0000 C CNN
F 1 "10K" V 7875 3425 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 7950 3425 50  0001 C CNN
F 3 "~" H 7950 3425 50  0001 C CNN
	1    7950 3425
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR09
U 1 1 60641864
P 7950 3600
F 0 "#PWR09" H 7950 3350 50  0001 C CNN
F 1 "GND" H 7955 3427 50  0000 C CNN
F 2 "" H 7950 3600 50  0001 C CNN
F 3 "" H 7950 3600 50  0001 C CNN
	1    7950 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 3025 7950 2925
Connection ~ 7950 2925
Wire Wire Line
	7950 2925 8300 2925
Wire Wire Line
	7950 3325 7950 3275
Wire Wire Line
	7950 3600 7950 3525
Text GLabel 7825 3275 0    50   Input ~ 0
VIN
Wire Wire Line
	7825 3275 7950 3275
Connection ~ 7950 3275
Wire Wire Line
	7950 3275 7950 3225
Wire Wire Line
	8500 3275 8500 3400
Connection ~ 8500 3275
Wire Wire Line
	8500 3600 8500 3725
$Comp
L power:GND #PWR010
U 1 1 6068A45E
P 8500 4275
F 0 "#PWR010" H 8500 4025 50  0001 C CNN
F 1 "GND" H 8505 4102 50  0000 C CNN
F 2 "" H 8500 4275 50  0001 C CNN
F 3 "" H 8500 4275 50  0001 C CNN
	1    8500 4275
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 4125 8500 4275
$Comp
L Device:R_Small R30
U 1 1 60696D26
P 8875 4100
F 0 "R30" V 8950 4100 50  0000 C CNN
F 1 "1K" V 8800 4100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 8875 4100 50  0001 C CNN
F 3 "~" H 8875 4100 50  0001 C CNN
	1    8875 4100
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R31
U 1 1 60697FCD
P 9125 3925
F 0 "R31" V 9200 3925 50  0000 C CNN
F 1 "1K" V 9050 3925 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9125 3925 50  0001 C CNN
F 3 "~" H 9125 3925 50  0001 C CNN
	1    9125 3925
	0    -1   -1   0   
$EndComp
$Comp
L Device:D_Zener_Small D25
U 1 1 606991A3
P 9450 3925
F 0 "D25" H 9450 3720 50  0000 C CNN
F 1 "PDZ11B" H 9450 3811 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-323_HandSoldering" V 9450 3925 50  0001 C CNN
F 3 "~" V 9450 3925 50  0001 C CNN
	1    9450 3925
	-1   0    0    1   
$EndComp
Text GLabel 9675 3925 2    50   Input ~ 0
VIN
$Comp
L power:GND #PWR011
U 1 1 6069AA35
P 8875 4275
F 0 "#PWR011" H 8875 4025 50  0001 C CNN
F 1 "GND" H 8880 4102 50  0000 C CNN
F 2 "" H 8875 4275 50  0001 C CNN
F 3 "" H 8875 4275 50  0001 C CNN
	1    8875 4275
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3925 8875 3925
Wire Wire Line
	9225 3925 9350 3925
Wire Wire Line
	9550 3925 9675 3925
Wire Wire Line
	8875 4000 8875 3925
Connection ~ 8875 3925
Wire Wire Line
	8875 3925 9025 3925
Wire Wire Line
	8875 4275 8875 4200
$Comp
L conn:Jack-DC J3
U 1 1 606ED217
P 6225 3675
F 0 "J3" H 6304 4000 50  0000 C CNN
F 1 "Jack-DC" H 6304 3909 50  0000 C CNN
F 2 "Connect:BARREL_JACK" H 6275 3635 50  0001 C CNN
F 3 "~" H 6275 3635 50  0001 C CNN
	1    6225 3675
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0101
U 1 1 606ED90E
P 6650 3475
F 0 "#PWR0101" H 6650 3325 50  0001 C CNN
F 1 "+12V" H 6665 3648 50  0000 C CNN
F 2 "" H 6650 3475 50  0001 C CNN
F 3 "" H 6650 3475 50  0001 C CNN
	1    6650 3475
	1    0    0    -1  
$EndComp
Wire Wire Line
	6525 3575 6650 3575
Wire Wire Line
	6650 3575 6650 3475
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 606FB51A
P 6650 3575
F 0 "#FLG0101" H 6650 3650 50  0001 C CNN
F 1 "PWR_FLAG" V 6650 3703 50  0000 L CNN
F 2 "" H 6650 3575 50  0001 C CNN
F 3 "~" H 6650 3575 50  0001 C CNN
	1    6650 3575
	0    1    1    0   
$EndComp
Connection ~ 6650 3575
$Comp
L power:GND #PWR0102
U 1 1 606FC308
P 6650 3850
F 0 "#PWR0102" H 6650 3600 50  0001 C CNN
F 1 "GND" H 6655 3677 50  0000 C CNN
F 2 "" H 6650 3850 50  0001 C CNN
F 3 "" H 6650 3850 50  0001 C CNN
	1    6650 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6525 3675 6650 3675
Wire Wire Line
	6650 3675 6650 3775
Wire Wire Line
	6525 3775 6650 3775
Connection ~ 6650 3775
Wire Wire Line
	6650 3775 6650 3850
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 607174D5
P 6650 3775
F 0 "#FLG0102" H 6650 3850 50  0001 C CNN
F 1 "PWR_FLAG" V 6650 3903 50  0000 L CNN
F 2 "" H 6650 3775 50  0001 C CNN
F 3 "~" H 6650 3775 50  0001 C CNN
	1    6650 3775
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 60718A1B
P 9900 1750
F 0 "#FLG0103" H 9900 1825 50  0001 C CNN
F 1 "PWR_FLAG" V 9900 1878 50  0000 L CNN
F 2 "" H 9900 1750 50  0001 C CNN
F 3 "~" H 9900 1750 50  0001 C CNN
	1    9900 1750
	0    1    1    0   
$EndComp
$EndSCHEMATC
